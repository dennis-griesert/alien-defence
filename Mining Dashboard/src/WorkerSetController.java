import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.text.Text;

public class WorkerSetController implements Initializable{

    @FXML
    private ToggleButton bt380;

    @FXML
    private TextField tfFury;

    @FXML
    private ToggleButton btFury;

    @FXML
    private ToggleButton bt480;

    @FXML
    private TextField tf470;

    @FXML
    private TextField tf480;

    @FXML
    private ToggleButton bt470;

    @FXML
    private TextField tf570;

    @FXML
    private ToggleButton bt580;

    @FXML
    private TextField tf5600XT;

    @FXML
    private TextField tfVega64;

    @FXML
    private ToggleButton btVega56;

    @FXML
    private ToggleButton btVega64;

    @FXML
    private TextField tfVega56;

    @FXML
    private TextField tf580;

    @FXML
    private ToggleButton bt570;

    @FXML
    private ToggleButton btVII;

    @FXML
    private TextField tf6800;

    @FXML
    private TextField tfVII;

    @FXML
    private ToggleButton bt5700XT;

    @FXML
    private ToggleButton bt5700;

    @FXML
    private TextField tf5700XT;

    @FXML
    private TextField tf5700;

    @FXML
    private ToggleButton bt5600XT;

    @FXML
    private ToggleButton bt1060;

    @FXML
    private TextField tf1070;

    @FXML
    private TextField tf1060;

    @FXML
    private ToggleButton bt1050Ti;

    @FXML
    private ToggleButton bt6800XT;

    @FXML
    private TextField tf1050Ti;

    @FXML
    private TextField tf6800XT;

    @FXML
    private ToggleButton bt6800;

    @FXML
    private TextField tf1660;

    @FXML
    private ToggleButton bt1080Ti;

    @FXML
    private TextField tf1080Ti;

    @FXML
    private ToggleButton bt1080;

    @FXML
    private ToggleButton bt1070Ti;

    @FXML
    private TextField tf1080;

    @FXML
    private TextField tf1070Ti;

    @FXML
    private ToggleButton bt1070;

    @FXML
    private TextField tf2070;

    @FXML
    private ToggleButton bt2060;

    @FXML
    private TextField tf2060;

    @FXML
    private ToggleButton bt1660S;

    @FXML
    private ToggleButton bt1660Ti;

    @FXML
    private TextField tf1660S;

    @FXML
    private TextField tf1660Ti;

    @FXML
    private ToggleButton bt1660;

    @FXML
    private TextField tf3070;

    @FXML
    private ToggleButton bt3060Ti;

    @FXML
    private TextField tf3060Ti;

    @FXML
    private ToggleButton bt2080Ti;

    @FXML
    private ToggleButton bt2080;

    @FXML
    private TextField tf2080Ti;

    @FXML
    private TextField tf2080;

    @FXML
    private ToggleButton bt2070;

    @FXML
    private ToggleButton bt3090;

    @FXML
    private ToggleButton bt3080;

    @FXML
    private TextField tf3090;

    @FXML
    private ToggleButton bt3070;

    @FXML
    private TextField tf380;

    @FXML
    private TextField tf3080;

    @FXML
    private TextArea taauswahl;

    @FXML
    private TextArea taeingestellt;

    @FXML
    private Button btnehmen;

    @FXML
    private Button btreset;

    @FXML
    private Text tfTitel;

    @FXML
    void set1050Tiworker(ActionEvent event) {

    }

    @FXML
    void set1060worker(ActionEvent event) {

    }

    @FXML
    void set1070Tiworker(ActionEvent event) {

    }

    @FXML
    void set1070worker(ActionEvent event) {

    }

    @FXML
    void set1080Tiworker(ActionEvent event) {

    }

    @FXML
    void set1080worker(ActionEvent event) {

    }

    @FXML
    void set1660Sworker(ActionEvent event) {

    }

    @FXML
    void set1660Tiworker(ActionEvent event) {

    }

    @FXML
    void set1660worker(ActionEvent event) {

    }

    @FXML
    void set2060worker(ActionEvent event) {

    }

    @FXML
    void set2070worker(ActionEvent event) {

    }

    @FXML
    void set2080Tiworker(ActionEvent event) {

    }

    @FXML
    void set2080worker(ActionEvent event) {

    }

    @FXML
    void set3060Tiworker(ActionEvent event) {

    }

    @FXML
    void set3070worker(ActionEvent event) {

    }

    @FXML
    void set3080worker(ActionEvent event) {

    }

    @FXML
    void set3090worker(ActionEvent event) {

    }

    @FXML
    void set380worker(ActionEvent event) {

    }

    @FXML
    void set470worker(ActionEvent event) {

    }

    @FXML
    void set480worker(ActionEvent event) {

    }

    @FXML
    void set5600XTworker(ActionEvent event) {

    }

    @FXML
    void set5700XTworker(ActionEvent event) {

    }

    @FXML
    void set5700worker(ActionEvent event) {

    }

    @FXML
    void set570worker(ActionEvent event) {

    }

    @FXML
    void set580worker(ActionEvent event) {

    }

    @FXML
    void set6800XTworker(ActionEvent event) {

    }

    @FXML
    void set6800worker(ActionEvent event) {

    }

    @FXML
    void setFuryworker(ActionEvent event) {

    }

    @FXML
    void setVIIworker(ActionEvent event) {

    }

    @FXML
    void setVega56worker(ActionEvent event) {

    }

    @FXML
    void setVega64worker(ActionEvent event) {

    }

    @FXML
    void setconfirm(ActionEvent event) {

    }

    @FXML
    void setreset(ActionEvent event) {

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
