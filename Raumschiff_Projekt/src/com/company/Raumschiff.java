package com.company;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Die Klasse des Raumschiffes umfasst den jeweiligen Konstruktor sowie alle ben�tigten Methoden 
 */
public class Raumschiff {
	
    int huelleInProzent;
    int lebenserhaltungssystemeInProzent;
    int androidenAnzahl;
	    
    int photonentorpedoAnzahl;
    int energieversorgungInProzent;
    int schildeInProzent;
  
    
    String schiffsname;
    ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
    Methoden m = new Methoden();
    
    
    
    


    /**
     * Konstruktor Raumschiff  
     */
    
    
    
    
    
    public Raumschiff(int photonentorpedoAnzahl, 
    		int energieversorgungInProzent,
    		int schildeInProzent, int huelleInProzent, 
    		int lebenserhaltungssystemeInProzent,
    		int androidenAnzahl, String schiffsname) {
    	
    				this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    				this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    				this.androidenAnzahl = androidenAnzahl;
    				this.schiffsname = schiffsname; 
    				this.energieversorgungInProzent = energieversorgungInProzent;
    				this.schildeInProzent = schildeInProzent;
    				this.huelleInProzent = huelleInProzent;
    
    }

    /**
     * Zustand des Raumschiffes
     */
    
    public void zustandRaumschiff() {
    	
    	 System.out.printf("%n%2s%7s%24s%12s%10s%n", "|", "Schild", "|", this.getSchildeInProzent() + "%", "|");
    	 
         this.m.strich();
         
         System.out.printf("%n%2s%6s%25s%12s%10s%n", "|", "Hülle", "|", this.getSchildeInProzent() + "%", "|");
         
         this.m.strich();
         
         System.out.printf("%n%2s%24s%7s%12s%10s%n", "|", "Lebenserhaltungssysteme", "|", this.getLebenserhaltungssystemeInProzent() + "%", "|");
         
         this.m.strich();
         
         System.out.println("\n\nDerzeitiger Zustand der: " + this.getSchiffsname());
         
         this.m.strich();
        
         System.out.printf("%n%2s%16s%15s%10s%12s%n", "|", "Photonentorpedo", "|", this.getPhotonentorpedoAnzahl(), "|");
         
         this.m.strich();
        
         System.out.printf("%n%2s%20s%11s%10s%12s%n", "|", "Reperatur-Androiden", "|", this.getAndroidenAnzahl(), "|");
         
         this.m.strich();
        
         System.out.printf("%n%2s%18s%13s%12s%10s%n", "|", "Energieversorgung", "|", this.getEnergieversorgungInProzent() + "%", "|");
         
         this.m.strich();
        
       
    }

 
    public void addLadung(Ladung ladung) {
        this.ladungsverzeichnis.add(ladung);
    }


    public void ladungsverzeichnisAusgeben() {
        System.out.println("\n\n\nLadungsverzeichnis der " + this.getSchiffsname() + ":");
        Iterator<Ladung> var1 = this.ladungsverzeichnis.iterator();

        while(var1.hasNext()) {
            Ladung ladungsverzeichnis = (Ladung)var1.next();
            this.m.strich();
            if (ladungsverzeichnis.getBezeichner().length() <= 15) {
                System.out.printf("%n%2s%14s%5s%24s%10s%n", "|", "Bezeichner", "|", ladungsverzeichnis.getBezeichner(), "|");
            } else {
                System.out.printf("%n%2s%14s%5s%30s%4s%n", "|", "Bezeichner", "|", ladungsverzeichnis.getBezeichner(), "|");
            }

            this.m.strich();
            System.out.println("%n%2s%11s%8s%18s%16s%n" + "|" + "Menge" + "|" + ladungsverzeichnis.getMenge() + "|");
            
            this.m.strich();
            System.out.println(" \n ");
        }

    }
    
    /**
     * @para s Raumschiff "s" gibt das jeweilige Raumschiff an, welches als Ziel definiert wird
     */
    
    
    public void photonentorpedosSchiessen(Raumschiff s) {
        if (this.getPhotonentorpedoAnzahl() != 0) {
            this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() - 1);
            this.broadcastKommunikator.add("Photonentorpedo abgeschossen\n");
            this.treffer(s);
        } else {
            this.broadcastKommunikator.add("-=*Click*=-\n");
        }

    }

    /**
     * @para s welches Ziel definiert anvisiert wird
     */
    
    
    public void phaserkanoneSchiessen(Raumschiff s) {
        if (this.getEnergieversorgungInProzent() >= 40) {
        	
            this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() - 40);
            
            this.broadcastKommunikator.add("Phaserkanone abgeschossen\n");
            
            this.treffer(s);
            
        } else {
        	
            this.broadcastKommunikator.add("-=*Click*=-\n");
            
        }

    }


    /**
     * @para s welches Raumschiff besch�digt
     */
    public void treffer(Raumschiff s) {
    	
        System.out.println("\n" + s.getSchiffsname() + " wurde getroffen!");
        
        if (s.getSchildeInProzent() <= 0) {
        	
            s.setSchildeInProzent(0);
            
            s.setLebenserhaltungssystemeInProzent(0);
            
            s.nachrichtAnAlle("Lebenserhaltungssysteme wurden vernichtet!");
            
        } else if (s.getSchildeInProzent() <= 0) {
        	
            s.setSchildeInProzent(s.getSchildeInProzent() - 40);
            
            s.setEnergieversorgungInProzent(s.getEnergieversorgungInProzent() - 40);
            
        } else {
        	
            s.setSchildeInProzent(s.getSchildeInProzent() - 40);
            
            if (s.getSchildeInProzent() <= 0) {
            	
                s.setSchildeInProzent(0);
                
            }
            
        }

    }

    /**
     * Keine Torpedos, dann  Nachricht in Konsole.
     * 
     * @para anzahlTorpedos gew�nschte Zahl der Torpedos.
     */
    
    public void photonentorpedosLaden(int anzahlTorpedos) {
    	
        boolean vorhanden = false;
        
        Iterator<Ladung> var3 = this.ladungsverzeichnis.iterator();

        while(var3.hasNext()) {
            Ladung ladung = (Ladung)var3.next();
          
            if (ladung.bezeichner.equals("Photonentorpedo")) {
            	
                vorhanden = true;
                
                if (anzahlTorpedos >= ladung.menge) {
                	
                    this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + ladung.menge);
                    System.out.println("\n" + ladung.menge + " Photonentorpedo(s) übrig!");
                    System.out.println(ladung.menge + " Photonentorpedo(s) eingesetzt");
                    ladung.setMenge(0);
                    
                } 
                
                else {
                	
                    ladung.setMenge(ladung.menge - anzahlTorpedos);
                    this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedos);
                    System.out.println("\n" + anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
                    
                }
                
                break;
            }
        }

        if (!vorhanden) {
        	
            System.out.println("\nKeine Photonentorpedos gefunden!");
            this.nachrichtAnAlle("-=*Click*=-");
            
        }

    }


    /**
     * @para message wird ArrayList "broadcastk..." hinzugef�gt 
     */
    public void nachrichtAnAlle(String message) {
    	
        this.broadcastKommunikator.add(message);
        
    }


    public void eintraegeLogbuchZurueckgeben() {
    	
        System.out.println("\nLogbucheinträge:\n");
        
        Iterator<String> var1 = this.broadcastKommunikator.iterator();

        while(var1.hasNext()) {
        	
            String s = (String)var1.next();
            
            System.out.println("Nachricht an alle: " + s);
            
        }

    }
    public void ladungsverzeichnisAufraeumen() {
    	
        this.ladungsverzeichnis.removeIf((ladung) -> {
        	
            return ladung.menge == 0;
            
        });
    }

    /**
     * @para schutzschilde Bool Wert der Reperatur
     * @para energieversorgung Bool Wert Reperatur 
     * @para schiffshuelle Bool Wert Reperatur 
     * 
     * 
     * @para anzahlDroiden Anzahl ben�tigter Reperatur-Androiden
     */
    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, long l) {
        Random r = new Random();
        int zufallszahl = r.nextInt(101);
        int reperatur = 0;
        int zaehler = 0;
        if (schutzschilde) {
            ++zaehler;
        }

        if (energieversorgung) {
            ++zaehler;
        }

        if (schiffshuelle) {
            ++zaehler;
        }

        if (l >= this.androidenAnzahl && zaehler > 0) {
            reperatur = zufallszahl * this.androidenAnzahl / zaehler;
        } else if (zaehler > 0) {
            reperatur = (int) (zufallszahl * l / zaehler);
        } else {
            System.out.println("Keine Reperaturangaben erhalten!");
        }

        if (schutzschilde) {
            System.out.println("schutzschilde reparieren");
            this.setSchildeInProzent(Math.min(this.getSchildeInProzent() + reperatur, 100));
        }

        if (energieversorgung) {
            System.out.println("energieversorgung reparieren");
            this.setEnergieversorgungInProzent(Math.min(this.getEnergieversorgungInProzent() + reperatur, 100));
        }

        if (schiffshuelle) {
            System.out.println("schiffshuelle reparieren");
            this.setSchildeInProzent(Math.min(this.getSchildeInProzent() + reperatur, 100));
        }

    }


    
    
    private void setSchildeInProzent(long min) {
		// TODO Auto-generated method stub
		
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
    	
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        
    }

  
    public int getEnergieversorgungInProzent() {
    	
        return this.energieversorgungInProzent;
    }
    
    public int getPhotonentorpedoAnzahl() {
    	
        return this.photonentorpedoAnzahl;
    }

  

   
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
    	
        this.energieversorgungInProzent = energieversorgungInProzent;
        
    }
    
    public void setAndroidenAnzahl(int androidenAnzahl) {
    	
        this.androidenAnzahl = androidenAnzahl;
        
    }


    public String getSchiffsname() {
    	
        return this.schiffsname;
        
    }

    public void setSchiffsname(String schiffsname) {
    	
        this.schiffsname = schiffsname;
        
    }

    public long getSchildeInProzent() {
    	
        return this.schildeInProzent;
        
    }

 
 
    }

 
    public long getLebenserhaltungssystemeInProzent() {
    	
        return this.lebenserhaltungssystemeInProzent;
        
    }

   
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
    	
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        
    }

   
    public long getAndroidenAnzahl() {
    	
        return this.androidenAnzahl;
        
    }


     
 
}
