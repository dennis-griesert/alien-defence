package com.company;

public class Main {

    public static void main(String[] args) {
    	
        Raumschiff r_1 = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        Raumschiff r_2 = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        Raumschiff r_3 = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
        Ladung r1_l1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung r1_l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
        Ladung r2_l1 = new Ladung("Borg-Schrott", 5);
        Ladung r2_l2 = new Ladung("Rote Materie", 2);
        Ladung r2_l3 = new Ladung("Plasma-Waffe", 50);
        Ladung r3_l1 = new Ladung("Forschungssonde", 35);
        Ladung r3_l2 = new Ladung("Photonentorpedo", 3);
        r_1.addLadung(r1_l1);
        r_1.addLadung(r1_l2);
        r_2.addLadung(r2_l1);
        r_2.addLadung(r2_l2);
        r_2.addLadung(r2_l3);
        r_3.addLadung(r3_l1);
        r_3.addLadung(r3_l2);
        r_1.zustandRaumschiff();
        r_2.zustandRaumschiff();
        r_3.zustandRaumschiff();
        r_1.photonentorpedosSchiessen(r_2);
        r_2.phaserkanoneSchiessen(r_1);
        r_3.nachrichtAnAlle("Gewalt ist nicht logisch");
        r_1.zustandRaumschiff();
        r_2.ladungsverzeichnisAusgeben();
        r_3.reparaturDurchfuehren(true, true, true, r_3.getAndroidenAnzahl());
        r_3.photonentorpedosLaden(r3_l2.getMenge());
        r_3.ladungsverzeichnisAufraeumen();
        r_1.photonentorpedosSchiessen(r_2);
        r_1.photonentorpedosSchiessen(r_2);
        r_1.zustandRaumschiff();
        r_1.ladungsverzeichnisAusgeben();
        r_2.zustandRaumschiff();
        r_2.ladungsverzeichnisAusgeben();
        r_3.zustandRaumschiff();
        r_3.ladungsverzeichnisAusgeben();
    }
}
