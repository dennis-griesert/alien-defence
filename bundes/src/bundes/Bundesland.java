package bundes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Bundesland {
	int a;
	
	public Bundesland(int pPLZ) {	
		calculateBundesland(pPLZ);
	}
	
	
	private String calculateBundesland(int pPLZ) {
		
//		String[] bundeslaender = getBundeslaender();
		
		ArrayList <Integer> plz2 = getPostleitzahlen();
		int[] plz = plz2.stream().mapToInt(Integer::intValue).toArray();
		ArrayList<String> bund = getBundeslaender();
		
		int s = 0;	
		String bundesland1 = "";
		
				 for (int i=1;i<plz.length-1;i++,i++) {
					 int f = i - 1;
					    if (plz[i]!=0) {
					    	if (pPLZ < plz[i] & plz[f] < pPLZ | pPLZ == plz[i] | pPLZ == plz[f])
					    	  s = (int) i / 2;
					    	System.out.println(s);
					    	bundesland1 = bund.get(s);
					    	
					    	
					    }
					  }
		
		
				 System.out.print(bundesland1);
		return bundesland1;
	}


	private ArrayList<Integer> getPostleitzahlen() {
		ArrayList<String> bundeslaender = new ArrayList<String>();
		ArrayList<Integer> plz = new ArrayList<Integer>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/plzToBlnd.txt"));
			String zeile = null;
		        in.readLine();
			while ((zeile = in.readLine()) != null) {
				String[] parts=zeile.split(" |-");
				plz.add(Integer.valueOf(parts[0]));
				plz.add(Integer.valueOf(parts[1]));
				bundeslaender.add(parts[2]);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return plz;
	}


	private ArrayList<String> getBundeslaender() {
		ArrayList<String> bundeslaender = new ArrayList<String>();
		ArrayList<Integer> plz = new ArrayList<Integer>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/plzToBlnd.txt"));
			String zeile = null;
		        in.readLine();
			while ((zeile = in.readLine()) != null) {
				String[] parts=zeile.split(" |-");
				plz.add(Integer.valueOf(parts[0]));
				plz.add(Integer.valueOf(parts[1]));
				bundeslaender.add(parts[2]);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bundeslaender;
	}
	
}
